WARNING this procedure erases all data in your FairPhone.

```
git clone https://gitlab.com/fposos/builder.git fposos_builder
cd fposos_builder
```

Build docker image.

```
./fposos_build_docker_image
```

Run docker container.

```
./fposos_run_docker_container
```

Inside the docker container build FairPhone OpenSource OperatingSystem.

```
fposos_build
```

Accept BLOBs licence.

Wait and be patient.

Shutdown your FairPhone.

Hold volume down button and press power button.

Connect your FairPhone to your computer via USB cable.

Wait the reboot.
